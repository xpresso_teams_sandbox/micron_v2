from __future__ import print_function

import os
import pickle

import tensorflow as tf
from app.create_filename import create_filename
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import LSTM, Dropout
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from keras.utils import multi_gpu_model
from numpy import array
from tensorflow.python.client import device_lib
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent

config = tf.ConfigProto()
config.intra_op_parallelism_threads = 500
config.inter_op_parallelism_threads = 500

cur_work_dir = os.getcwd()


class LstmTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="LstmTrain")
        """ Initialize all the required constansts and data her """

    def start(self, run_name, config, master_path, type, my_logger):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)

        my_logger.info(device_lib.list_local_devices())
        if type == "_train_":
            X_train, y_train, vocab_size, max_length = padding(master_path, config, type, my_logger)

            use_gpu_bool = config["lstm"]["use_gpu"]
            load_weight_bool = config['lstm']['load_weight_bool']

            my_logger.info('Build model...')
            model = Sequential()
            model.add(Embedding(vocab_size + 1, 100, input_length=max_length - 1))
            model.add(Dropout(config['lstm']['dropout']))
            model.add(LSTM(config['lstm']['lstm_no']))
            model.add(Dense(vocab_size + 1, activation='softmax'))
            my_logger.info(model.summary())

            if use_gpu_bool:
                model = multi_gpu_model(model)

            if load_weight_bool:
                model.load_weights(os.path.abspath(
                    os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                 config['lstm']['model_save_path'], config['lstm']['load_weight_model_name'])))

            # try using different optimizers and different optimizer configs
            '''
            model.compile(loss='sparse_categorical_crossentropy',
                          optimizer='adam',
                          metrics=['accuracy'])
            '''
            my_logger.info('Train...')

            model.compile(loss='sparse_categorical_crossentropy', optimizer=config['lstm']['optimizer'],
                          metrics=["accuracy"])
            history_callback = model.fit(X_train[1:2700], y_train[1:2700],
                                         validation_split=config['lstm']['validation_split'],
                                         batch_size=config['lstm']['batch_size'], epochs=config['lstm']['epochs'])
            history_callback = history_callback.history

            accuracy = history_callback['accuracy']
            loss = history_callback['loss']
            val_accuracy = history_callback['val_accuracy']
            val_loss = history_callback['val_loss']
            my_logger.info(f"Training Accuray for each epoch : {str(accuracy)} \n")
            my_logger.info(f"Training Loss for each epoch : {str(loss)}\n")
            my_logger.info(f"Validation Accuray for each epoch : {str(val_accuracy)}\n")
            my_logger.info(f"Validation Loss for each epoch : {str(val_loss)}\n")

            self.send_metrics(str(accuracy), str(loss), str(val_accuracy), str(val_loss))

            model_name = "model" + create_filename(config) + ".h5"
            my_logger.info("Saving model...")
            model.save(os.path.abspath(
                os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                             str(config['lstm']['model_save_path']), str(model_name))))

        elif type == "_test_":
            X_test, y_test, vocab_size, max_length = padding(master_path, config, type, my_logger)
            model_name = "model" + create_filename(config) + ".h5"
            if os.path.exists(os.path.abspath(
                    os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                 config['lstm']['model_save_path'], model_name))):
                model = load_model(os.path.abspath(
                    os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                                 str(config['lstm']['model_save_path']), str(model_name))))

                model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=["accuracy"])
                score = model.evaluate(X_test, y_test)
                my_logger.info(f"Test score and accuracy : {str(score)}")
            else:
                my_logger.info("No model found at specified directory")

        self.completed()

    def send_metrics(self, accuracy, loss, val_accuracy, val_loss):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {
                "Accuracy": accuracy,
                "Loss": loss,
                "Value_Accuracy": val_accuracy,
                "Value_Loss": val_loss
            }
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)


def padding(master_path, config, type, my_logger):
    sequence_filename = "sequence" + type + create_filename(config) + ".pickle"
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], sequence_filename)), 'rb') as handle:
        sequences = pickle.load(handle)
    handle.close()

    if type == "_train_":
        max_length = max([len(seq) for seq in sequences])
        with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                               config['lstm']['pickle_path'], max_length_filename)), 'wb') as handle:
            pickle.dump(max_length, handle, protocol=pickle.HIGHEST_PROTOCOL)
        handle.close()

    elif type == "_test_":
        with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                               config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
            max_length = pickle.load(handle)
        handle.close()

    my_logger.info("Max length sequence : {max_length}")

    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    vocab_size = len(tokenizer.word_index)
    my_logger.info(f"voacb size: {str(vocab_size)}")

    sequences = pad_sequences(sequences, maxlen=max_length, padding='pre')
    sequences = array(sequences)
    X, y = sequences[:, :-1], sequences[:, -1]
    return X, y, vocab_size, max_length
