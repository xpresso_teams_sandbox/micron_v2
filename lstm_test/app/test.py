import json
import os
import sys

from app.model_train import LstmTest
from app.sequence_generation import sequence_generation
from xpresso.ai.core.logging.xpr_log import XprLogger

my_logger = XprLogger("lstm_test")
cur_work_dir = os.getcwd()

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)

use_gpu_bool = config['lstm']['use_gpu']
if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

if __name__ == '__main__':
    sequence_generation(config, master_path, "_test_", my_logger)
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    else:
        run_name = ""
    LstmTest().start(run_name=run_name, config=config, master_path=master_path, type="_test_", my_logger=my_logger)
