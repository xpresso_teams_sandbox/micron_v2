"""
    Bayesian network module
"""

__author__ = "Sanyog Vyawahare"

import json
import operator
import os
import sys
import pickle
from collections import Counter

import numpy as np
from nltk import ngrams
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

logging = XprLogger("bn_train")


# Folder creating function
def create_folders(dirnames, pathname):
    """ Creating folder if not present """
    for dirname in dirnames:
        folder_name = pathname + dirname
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)


# Model saving function
def save_model(data, filename):
    """ Saving graph """
    output_dict = open(filename, 'wb')
    pickle.dump(data, output_dict)
    output_dict.close()
    return


# Bayesian network class
class BNetworkTrain(AbstractPipelineComponent):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        super().__init__(name="BNetworkTrain")
        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.mountedpath = self.data_prep_param['mounted_path']
        self.inputfile = self.data_prep_param['input_file']

        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.folders = self.data_prep_param['folders']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.trainfileseq = self.datapath + self.data_prep_param['train_file_seq']

        self.lookahead = self.data_model['lookahead_window']
        self.modelpath = self.mountedpath + self.data_model['model_path_bn']
        self.logpath = self.modelpath + self.data_prep_param['logfile_train']

        self.bn_split = self.modelpath + self.data_model['output_files']['bn_split']

        self.range_list = range(self.lookahead, 0, -1)
        self.lines = []

        self.my_logger = logging

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)

        self.my_logger.info("Process started")
        print("Process started")

        # Creating folders
        create_folders(self.folders, self.mountedpath)

        # Loading and parsing train file
        self.my_logger.info("Loading and parsing train file")
        print("Loading and parsing train file")
        self.load_train_file()

        # Building bayesian network model
        self.my_logger.info("Building bayesian network model")
        print("Building bayesian network model")
        self.train()

        self.completed()

    def send_metrics(self, data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"N_gram_Training": data}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def load_train_file(self):
        """ Reading file and dividing block access into sequences """
        with open(self.trainfileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = line_num
        with open(self.trainfileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info(f"loading {str(loading_val)}")
                    print(f"loading train data {str(loading_val)}")
                train_seq = line.replace("\n", "").split(" ")
                self.lines.append(train_seq)

    def gen_ngram_dict(self, ngram):
        """ Creating bayesian network graph """
        arrlist, main_dict = [], {}
        for line_num, lines in enumerate(self.lines):
            if (line_num + 1) % 1000 == 0:
                temp = round(line_num * 100.0 / len(self.lines), 2)
                self.my_logger.info(f"{temp}")
                self.send_metrics(temp)
            temp = list(ngrams(lines, ngram + 1))
            arrlist += temp
        count = Counter(map(tuple, arrlist))
        for key, val in count.items():
            temp_key = list(key)[-1]
            main_key = '_'.join([str(item) for item in list(key)[:-1]])
            # main_key = unicode(main_key, "utf-8")
            # main_key = main_key.encode(encoding="utf-8")
            if main_key in main_dict:
                main_dict[main_key][temp_key] = val
            else:
                main_dict[main_key] = {temp_key: val}
        for block_id in main_dict:
            temp = sorted(main_dict[block_id].items(), key=operator.itemgetter(1), reverse=True)
            main_dict[block_id] = [item[0] for item in temp]
        return main_dict

    def train(self):
        """ Training module """
        all_blocks = []
        for ioaccess in self.lines:
            all_blocks += ioaccess
        all_blocks = sorted(list(set(all_blocks)))

        temp_val = 0.000044 * len(self.lines) + 0.000145 * len(all_blocks) + 17
        estimated_time = np.ceil(temp_val * 1.1 / 60.0)
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)

        _ = [os.remove(self.modelpath + item) for item in os.listdir(self.modelpath) \
             if item.endswith('.pkl')]
        for item in self.range_list:
            self.my_logger.info(f"{str(item)}")
            print(f"training model {str(item)}")
            filename = self.bn_split.replace('VAL', str(item))
            temp_dict = self.gen_ngram_dict(item)
            save_model(temp_dict, filename)
            del temp_dict


if __name__ == "__main__":

    data_prep = BNetworkTrain()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
