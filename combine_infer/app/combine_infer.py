"""
    Combine inference service for three models lstm, probablistic graph and bayesian network
"""
from __future__ import print_function

import errno
import json
import math
import operator
import os
import shutil
import pickle

import numpy as np
from app.create_filename import create_filename
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Sanyog Vyawahare"

logging = XprLogger("combine_infer")

cur_work_dir = os.getcwd()

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

use_gpu_bool = config['lstm']['use_gpu']
num_of_gpu = len(config['lstm']['cuda_visible_devices'].split(","))

if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    if num_of_gpu == 1:
        config['lstm']['use_gpu'] = False
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)


def load_model_pg_bn(filename_pg_bn):
    """ Loading graph """
    input_dict = open(filename_pg_bn, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


def load_test_file_helper_combine(range_list, split, is_bn):
    master_dict = {}
    edges = {}
    for item in range_list:
        logging.info(f"Loading file {str(item)}")
        print("Loading file", str(item))
        filename_pg_bn = split.replace('VAL', str(item))
        if is_bn:
            master_dict[str(item)] = load_model_pg_bn(filename_pg_bn)
        else:
            edges.update(load_model_pg_bn(filename_pg_bn))
    if is_bn:
        return master_dict
    else:
        return edges


def get_pred(key_val, item3, modeldata):
    """ Getting prediction of sequence """
    pred = []
    key = '_'.join([str(item2) for item2 in key_val])
    print('Key', key)
    if key in modeldata[item3]:
        pred = modeldata[item3][key]
    print('pred', pred)
    return pred


def get_model_output(test_seq, number, lookahead, modeldata, weight):
    """ Get sequence and return the predicted sequence """
    test_seq_final = test_seq[-lookahead:]
    weight_predict = weight[1 - len(test_seq_final):] + [1]
    predict_dict = {}
    for ind, seq in enumerate(test_seq_final):
        if seq in modeldata:
            for block, prob in modeldata[seq][:number]:
                predict_dict[block] = round(prob * weight_predict[ind], 5)
    predict_dict_sorted = sorted(predict_dict.items(), key=operator.itemgetter(1), reverse=True)
    predicted = [item[0] for item in predict_dict_sorted][:number]
    return predicted


def get_model_output_bn(block, neighbor, lookahead, range_list, modeldata):
    """ Get sequence and return the predicted sequence """
    if len(block) < lookahead:
        block = [-1] * (lookahead - len(block)) + block
    else:
        block = block[-lookahead:]

    predict = []
    for item in range_list:
        key = list(block)[-item:]
        predict += get_pred(key, str(item), modeldata)
        if len(predict) >= neighbor:
            break
    return predict[:neighbor]


def pg_inference(request_body):
    data_model = config['model']['pg_bn']
    mountedpath = config['data_preprocess']['mounted_path'] + master_path
    modelpath = mountedpath + data_model['model_path_pg']

    lookahead = data_model['lookahead_window']
    weight = [round(math.exp(val * -0.65), 5) for val in range(lookahead - 1, 0, -1)]
    range_list = range(1, lookahead + 1)
    pg_split = modelpath + data_model['output_files']['pg_split']

    logging.info("Process started")
    print("Process started")

    logging.info("Model loading... ")
    print("Model loading... ")
    modeldata = load_test_file_helper_combine(range_list, pg_split, False)
    logging.info("Model loading Completed")
    print("Model loading Completed")

    try:
        block = request_body['inputseq']
        neighbor = int(request_body['topk'])
        block = block.split()
        logging.info(f'Input {block}')
        logging.info(f'TopK {neighbor}')
        resp_seq = get_model_output(block, neighbor, lookahead, modeldata, weight)
        logging.info(f'Output {resp_seq}')
        print(f'Block_Sequence: {resp_seq}')
    except Exception:
        final_output = "Internal Server Error"
        logging.info(f'Output {final_output}')
        return {'error': final_output}

    return {"prediction": resp_seq}


def bn_inference(request_body):
    data_model = config['model']['pg_bn']
    mountedpath = config['data_preprocess']['mounted_path'] + master_path
    modelpath = mountedpath + data_model['model_path_bn']
    lookahead = data_model['lookahead_window']
    range_list = range(lookahead, 0, -1)
    bn_split = modelpath + data_model['output_files']['bn_split']

    logging.info("Process started")
    print("Process started")

    logging.info("Model loading... ")
    print("Model loading... ")
    modeldata = load_test_file_helper_combine(range_list, bn_split, True)
    logging.info("Model loading Completed")
    print("Model loading Completed")

    try:
        block = request_body['inputseq']
        neighbor = int(request_body['topk'])
        block = block.split()
        logging.info(f'Input {block}')
        logging.info(f'TopK {neighbor}')
        resp_seq = get_model_output_bn(block, neighbor, lookahead, range_list, modeldata)
        logging.info(f'Output {resp_seq}')
        print(f'Block_Sequence: {resp_seq}')

    except Exception:
        final_output = "Internal Server Error"
        logging.info(f'Output {final_output}')
        return {'error': final_output}
    return {"prediction": resp_seq}


def lstm_inference(request_body):
    logging.info("Loading Tokenizer...")
    # Loading tokenizer
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    index_to_id = {v: k for k, v in tokenizer.word_index.items()}
    logging.info("Tokenizer Loading Completed")

    # Loading max length
    logging.info("Loading Max Sequence Length...")
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
        max_length = pickle.load(handle)
    handle.close()
    logging.info("Max Sequence Length Loading Completed")

    # Loading model
    logging.info("Loading model ...")
    model_name = "model" + create_filename(config) + ".h5"
    print("Model Name", model_name)
    if os.path.exists(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   config['lstm']['model_save_path'], model_name))):
        model = load_model(os.path.abspath(
            os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                         str(config['lstm']['model_save_path']), str(model_name))))

        use_gpu_bool = config["lstm"]["use_gpu"]
    logging.info("Model loading completed")
    try:
        input_sequence = request_body['inputseq']
        k = int(request_body['topk'])
        input_sequence = input_sequence.split()
        input_sequence_tokenized = tokenizer.texts_to_sequences([input_sequence])[0]
        input_sequence_padded = pad_sequences([input_sequence_tokenized], maxlen=max_length - 1, padding='pre')

        predicted_ids = np.ndarray.tolist(np.argsort(model.predict(input_sequence_padded), axis=1))[0]
        predicted_ids.reverse()
        resp_seq = list()

        for index, value in enumerate(predicted_ids):
            if index >= k:
                break
            resp_seq.append(index_to_id[value])
        return {"prediction": resp_seq}

    except Exception as e:
        return {'error': str(e)}


class CombineInfer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        self.copy_results(model_path, "/data")

    @staticmethod
    def copy_results(src, dst):
        if os.path.exists(dst):
            shutil.rmtree(dst)
        try:
            shutil.copytree(src, dst)
        except OSError as exc:
            if exc.errno == errno.ENOTDIR:
                shutil.copy(src, dst)
            else:
                raise

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        print("input_data", input_request)
        return input_request

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        if 'lstm' in os.environ["RUN_NAME"]:
            resp_seq = lstm_inference(input_request)
        elif 'pg' in os.environ["RUN_NAME"]:
            resp_seq = pg_inference(input_request)
        elif 'bn' in os.environ["RUN_NAME"]:
            resp_seq = bn_inference(input_request)
        else:
            resp_seq = "Invalid Parameter Model"

        return resp_seq

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        return output_response


if __name__ == "__main__":
    inference = CombineInfer()
    inference.load()
    inference.run_api(port=8000)

