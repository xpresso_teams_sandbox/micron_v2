"""
    Bayesian network module
"""
__author__ = "Sanyog Vyawahare"

import json
import os
import shutil, errno
import sys
import pickle

import numpy as np
import pandas as pd
from nltk import ngrams
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

logging = XprLogger("bn_test")


# Folder creating function
def create_folders(dirnames, pathname):
    """ Creating folder if not present """
    for dirname in dirnames:
        folder_name = pathname + dirname
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)


# Model loading function
def load_model(filename):
    """ Loading graph """
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


# Bayesian network class
class BNetworkTest(AbstractPipelineComponent):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        super().__init__(name="BNetworkTest")

        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.mountedpath = self.data_prep_param['mounted_path']
        self.inputfile = self.data_prep_param['input_file']

        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.folders = self.data_prep_param['folders']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.validatefileseq = self.datapath + self.data_prep_param['validate_file_seq']

        self.lookahead = self.data_model['lookahead_window']
        self.neighbors = self.data_model['topk_neighbor_predict_counts']
        self.modelpath = self.mountedpath + self.data_model['model_path_bn']
        self.resultfile = self.modelpath + self.data_prep_param['validate_result']
        self.logpath = self.modelpath + self.data_prep_param['logfile_test']

        self.bn_split = self.modelpath + self.data_model['output_files']['bn_split']

        self.range_list = range(self.lookahead, 0, -1)
        self.lines_test = []
        self.master_dict = {}

        self.test_seq_all = []
        self.act_ans_all = []
        self.pred_all = []
        self.hit_miss_all = []
        self.my_logger = logging

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)

        self.my_logger.info("Process started")
        print("Process started")

        # Computing ETA
        self.get_estimated_time()

        # Prepare test data
        self.my_logger.info("Prepare test data")
        print("Prepare test data")
        self.load_test_file()

        # Getting hit raio of the test data
        self.my_logger.info("Getting hit ratio of the test data")
        print("Getting hit ratio of the test data")
        self.test(self.neighbors)

        self.completed(push_exp=True)

    def send_metrics(self, result):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"Result": result}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        self.copy_results('/data', '/output')
        print("Data moved successfully to '/output' folder")

        super().completed(push_exp=push_exp)

    @staticmethod
    def copy_results(src, dst):
        if os.path.exists(dst):
            shutil.rmtree(dst)
        try:
            shutil.copytree(src, dst)
        except OSError as exc:
            if exc.errno == errno.ENOTDIR:
                shutil.copy(src, dst)
            else:
                raise

    def get_pred(self, key_val, item3):
        """ Getting prediction of sequence """
        pred = []
        key = '_'.join([str(item2) for item2 in key_val])
        if key in self.master_dict[item3]:
            pred = self.master_dict[item3][key]
        return pred

    def load_test_file_helper(self):
        """ helper function for test sequence generation """
        for item in self.range_list:
            self.my_logger.info(f"{item}")
            print("Loading model files ", str(item))
            filename = self.bn_split.replace('VAL', str(item))
            self.master_dict[str(item)] = load_model(filename)
        return self.master_dict['1']

    def load_test_file(self):
        """ Reading test file and dividing block access into sequences """
        active_list = self.load_test_file_helper()
        with open(self.validatefileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = line_num
        with open(self.validatefileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info(f"loading {str(loading_val)}")
                    print("loading test data", str(loading_val))
                validate_seq = line.replace("\n", "").split(" ")
                # validate_seq = [item for item in validate_seq if item in active_list]
                if len(validate_seq) <= 1:
                    continue
                if validate_seq:
                    self.lines_test.append(validate_seq)

    def test(self, neighbors):
        """ Calculating hit ratio """
        output = dict()
        arrlist, hit_all, total_all = [], 0, 0
        for line_num, lines in enumerate(self.lines_test):
            if (line_num + 1) % 100000 == 0:
                output_string = "getting ngram for test "
                output_string += str(round(line_num * 100.0 / len(self.lines_test), 2))
                self.my_logger.info(f"{output_string}")
                print(output_string)
            lines = [-1] * self.lookahead + lines
            arrlist += list(ngrams(lines, self.lookahead + 1))
        for line_num, item1 in enumerate(arrlist):
            hit_temp = 0
            if not (line_num + 1) % 100:
                output["Hit_ratio"] = round(float(hit_all) * 100.0 / float(total_all), 2)
                output["Neighbors"] = neighbors
                self.my_logger.info(f"{output}")
                self.send_metrics(output)
            predict = []
            val = str(list(item1)[-1])
            key_master = list(item1)[:-1]
            key_temp = [item for item in key_master if item != -1]
            if not key_temp:
                continue
            for item3 in self.range_list:
                key = key_master[-item3:]
                predict += self.get_pred(key, str(item3))
                if len(predict) >= neighbors:
                    break
            predicted = predict[:neighbors]
            if val in predicted:
                hit_temp = 1
            total_all += 1
            hit_all += hit_temp
            self.test_seq_all.append(key_temp)
            self.act_ans_all.append(val)
            self.hit_miss_all.append(hit_temp)
            if not predicted:
                self.pred_all.append(-1)
            else:
                self.pred_all.append(predicted[0])
        if total_all:
            ratio = round(float(hit_all) * 100.0 / float(total_all), 2)
        else:
            ratio = 0.0
        output_string = "Neighbor: " + str(neighbors)
        output_string += " Lookback_window: " + str(self.lookahead)
        output_string += " Cutting_window: " + str(self.cutting_window) + "\n"
        output_string += "Hit: " + str(hit_all)
        output_string += " Total: " + str(total_all)
        output_string += " Ratio: " + str(ratio) + "\n"
        self.my_logger.info(f"{output_string}")
        pred_data = {'test': self.test_seq_all, 'act': self.act_ans_all,
                     'pred': self.pred_all, 'hit': self.hit_miss_all}
        pred_data_frame = pd.DataFrame(pred_data)
        pred_data_frame.to_csv(self.resultfile, index=False, header=True)
        print(f"Run complete, Hit ratio {str(ratio)} %")

    def get_estimated_time(self):
        """ Getting estimated time """
        filesize = 0
        for item in self.range_list:
            filename = self.bn_split.replace('VAL', str(item))
            filesize += np.ceil(os.stat(filename).st_size / 1024.0 ** 2)
        estimated_time = np.ceil(filesize / (4500 * 60.0))
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)


if __name__ == "__main__":

    data_prep = BNetworkTest()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
